package com.baoling.common.constant;

/**
 * @author: weirdor
 * @since: 2023/12/11 22:51
 */
public interface CommonConstants {

    /**
     * 删除
     */
    String STATUS_DEL = "1";

    /**
     * 正常
     */
    String STATUS_NORMAL = "0";
}
