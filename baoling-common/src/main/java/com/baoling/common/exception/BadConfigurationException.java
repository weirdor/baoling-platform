package com.baoling.common.exception;

/**
 * 统一关于错误配置信息 异常
 * @author: weirdor
 * @since: 2023/12/12 08:12
 */
public class BadConfigurationException extends RuntimeException{

    public BadConfigurationException(String message) {
        super(message);
    }

    public BadConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadConfigurationException(Throwable cause) {
        super(cause);
    }

    public BadConfigurationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
