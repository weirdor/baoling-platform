package com.baoling.common.enums;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 用户类型
 * @author: weirdor
 * @since: 2023/12/12 08:13
 */
@Getter
@AllArgsConstructor
public enum UserType {

    /**
     * 运营端
     */
    SYS_USER("sys_user"),

    /**
     * 中心用户
     */
    CENTER_USER("center_user"),

    /**
     * 评估员
     */
    APP_USER("app_user");

    private final String userType;

    public static UserType getUserType(String str) {
        for (UserType value : values()) {
            if (StrUtil.contains(str, value.getUserType())) {
                return value;
            }
        }
        throw new RuntimeException("'UserType' not found By " + str);
    }
}