package com.baoling.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 菜单类型
 * @Author: Mr.ZS
 * @Date: 2023/12/15 17:01
 * @company 湖南葆龄科技有限公司
 **/
@Getter
@AllArgsConstructor
public enum MenuType {

    DIR(1), // 目录
    MENU(2), // 菜单
    BUTTON(3) // 按钮
    ;

    /**
     * 类型
     */
    private final Integer type;


}