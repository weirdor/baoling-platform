package com.baoling.admin.modules.system.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: Mr.ZS
 * @Date: 2023/12/15 16:23
 * @company 湖南葆龄科技有限公司
 **/
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SysMenuVo implements Serializable {

    private final static long serialVersionUID = 1L;

    private String id;

    private String parentId;

    private String name;


}
