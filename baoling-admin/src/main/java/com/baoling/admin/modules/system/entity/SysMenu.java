package com.baoling.admin.modules.system.entity;

import com.baoling.data.base.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author: Mr.ZS
 * @Date: 2023/12/15 15:57
 * @company 湖南葆龄科技有限公司
 **/
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "菜单")
@TableName("sys_menu")
public class SysMenu extends BaseEntity<SysMenu> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("菜单id")
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty("上级id, 0是顶级")
    private String parentId;

    @ApiModelProperty("菜单名称")
    private String title;

    @ApiModelProperty("菜单路由地址")
    private String path;

    @ApiModelProperty("菜单组件地址")
    private String component;

    @ApiModelProperty("菜单类型, 0菜单, 1按钮")
    private Integer menuType;

    @ApiModelProperty("排序号")
    private Integer sortNumber;

    @ApiModelProperty("权限标识")
    private String authority;

    @ApiModelProperty("打开位置")
    private String target;

    @ApiModelProperty("菜单图标")
    private String icon;

    @ApiModelProperty("图标颜色")
    private String color;

    @ApiModelProperty("是否隐藏, 0否, 1是(仅注册路由不显示左侧菜单)")
    private Integer hide;

    @ApiModelProperty("侧栏菜单选中的path")
    private String active;

    @ApiModelProperty("路由元信息")
    private String meta;
}
