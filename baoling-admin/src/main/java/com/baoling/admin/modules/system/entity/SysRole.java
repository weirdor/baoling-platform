package com.baoling.admin.modules.system.entity;

import com.baoling.data.base.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author: Mr.ZS
 * @Date: 2023/12/15 15:54
 * @company 湖南葆龄科技有限公司
 **/
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "角色")
@TableName("sys_role")
public class SysRole extends BaseEntity<SysRole> {

    private final static long serialVersionUID = 1L;

    @ApiModelProperty("角色id")
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty("角色名称")
    private String roleName;

    @ApiModelProperty("角色编码")
    private String code;

    @ApiModelProperty("数据权限")
    private String dataScope;

    @ApiModelProperty("角色描述")
    private String description;

}
