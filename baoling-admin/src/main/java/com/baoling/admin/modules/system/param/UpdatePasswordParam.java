package com.baoling.admin.modules.system.param;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 修改密码参数
 *
 * @Author: Mr.ZS
 * @Date: 2023/12/15 16:46
 * @company 湖南葆龄科技有限公司
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "修改密码参数")
public class UpdatePasswordParam implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("原始密码")
    private String oldPassword;

    @ApiModelProperty("新密码")
    private String password;

}
