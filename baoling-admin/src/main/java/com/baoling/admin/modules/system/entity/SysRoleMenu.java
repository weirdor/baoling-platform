package com.baoling.admin.modules.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author: Mr.ZS
 * @Date: 2023/12/15 16:00
 * @company 湖南葆龄科技有限公司
 **/
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "角色权限")
@TableName("sys_role_menu")
public class SysRoleMenu extends Model<SysRoleMenu> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty("角色id")
    private String roleId;

    @ApiModelProperty("菜单id")
    private String menuId;
}
