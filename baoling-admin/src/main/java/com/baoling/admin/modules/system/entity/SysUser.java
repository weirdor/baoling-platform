package com.baoling.admin.modules.system.entity;

import com.baoling.data.base.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author: Mr.ZS
 * @Date: 2023/12/15 15:02
 * @company 湖南葆龄科技有限公司
 **/
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "用户")
@TableName("sys_user")
public class SysUser extends BaseEntity<SysUser> {

    private final static long serialVersionUID = 1L;

    @ApiModelProperty("用户id")
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty("账号")
    private String username;

    @ApiModelProperty("密码")
    private String password;

    @ApiModelProperty("盐")
    private String salt;

    @ApiModelProperty("昵称")
    private String nickname;

    @ApiModelProperty("头像")
    private String avatar;

    @ApiModelProperty("状态 0锁定 1有效")
    private Integer status;

    @ApiModelProperty(hidden = true)
    @TableField(exist = false)
    private String statusText;

}
